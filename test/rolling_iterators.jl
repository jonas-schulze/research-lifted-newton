using DrWatson, Test

include(srcdir("rolling_iterators.jl"))

@testset "Rolling Iterators" begin
    n = 10
    k = 3

    count = 0
    f(x) = (count += 1; x)

    gen = (f(x) for x in 1:n)
    buf = rolling(gen, k)

    res = [copy(xs) for xs in buf] # copying collect
    @test res == [i:i+k-1 for i in 1:n-k+1]
    @test count == n
end
