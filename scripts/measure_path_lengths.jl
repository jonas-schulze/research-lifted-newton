using DrWatson, Parameters, ProgressMeter, JLD
using HomotopyContinuation: randseed
using PolynomialTestSystems: equations, cyclic

include(srcdir("cyclic_lifting.jl"))
include(srcdir("range_spanning_lifting.jl"))
include(srcdir("path-length.jl"))

function measure_path_length(config)
    @unpack n = config
    seed = randseed()

    F = equations(cyclic(n))
    lens = pathlens(F, seed=seed)
    res = @strdict n seed lens
    @tag! res
end

function measure_path_length_lifted(config)
    @unpack n, k, lifting = config
    seed = randseed()

    F = lifting(n, k)
    lens = pathlens(F, seed=seed)
    res = @strdict n k seed lens
    @tag! res
end

params = Dict(
    :n => collect(5:7),
    :k => collect(2:4),
    :lifting => [cyclic_lifting, range_spanning_lifting],
)

# Track progress:
n = dict_list_count(params) + length(params[:n])
progress = Progress(n, dt=0.5)

# Configure produce_or_load:
verbose = false
loadfile = false
suffix = "jld"
tag = false
kwargs = @dict verbose loadfile suffix tag

# Solve lifted systems:
for config in dict_list(params)
    @unpack lifting = config
    path = datadir("path-properties", "length", string(lifting))
    produce_or_load(path,
                    config,
                    measure_path_length_lifted;
                    kwargs...)
    next!(progress)
end

# Solve unlifted/raw systems:
for n in params[:n]
    path = datadir("path-properties", "length")
    config = @dict n
    produce_or_load(path,
                    config,
                    measure_path_length;
                    kwargs...)
    next!(progress)
end
