using DrWatson

include(srcdir("cyclic_lifting.jl"))
include(srcdir("solve_problem.jl"))

general_args = Dict(:n => collect(5:10),
                    :start_system => [:total_degree, :polyhedral])
dicts = dict_list(general_args)
files = map(dicts) do spec
  @unpack n = spec
  map(2:n÷2) do k
    @pack! spec = k
    F = cyclic_lifting(spec)
    @pack! spec = F

    file = datadir("cyclic_lifting", savename(spec, "jld"))
    @info "Saving $file ..."
    data = solve_problem(spec)
    @tagsave file data

    file
  end
end

vcat(files...)
