using DrWatson

include(srcdir("solve_raw_problem.jl"))

general_args = Dict(:n => collect(5:10),
                    :start_system => [:total_degree, :polyhedral])
dicts = dict_list(general_args)
map(dicts) do spec
  # Load using:
  #=
  data, fname = produce_or_load(datadir("raw"),
                                spec,
                                solve_raw_problem,
                                suffix = "jld",
                                loadfile = false,
                               )
  =#

  file = datadir("raw", savename(spec, "jld"))
  @info "Saving $file ..."
  data = solve_raw_problem(spec)
  @tagsave file data

  file
end
