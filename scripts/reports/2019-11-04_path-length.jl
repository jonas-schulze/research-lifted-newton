# # Path Length
#
# When lifting the problem, check whether the solver (using its default
# heuristics) needed fewer steps to track a path.

using DrWatson, Parameters, JLD, BSON
using HomotopyContinuation, DynamicPolynomials
using Statistics

using HomotopyContinuation: randseed
using DataFrames

using Gadfly

function describe_pathlengths(data, val, f)
    res = data["results"]
    sols = nonsingular(res)
    vals = [val(s) for s in sols]
    f(vals)
end

function collect_pathlengths!(dir; verbose=false)
    ## This has to be BSON, cf. https://github.com/JuliaIO/JLD.jl/issues/266
    fname  = joinpath(dirname(dir), "results_pathlengths_$(basename(dir)).bson")
    wl = ["spec"]
    sl = [
        :n => data -> data["spec"][:n],
        :k => data -> data["spec"][:k],
        :start_system => data -> data["spec"][:start_system],
        :accepted_steps_mean => data -> describe_pathlengths(data, s -> s.accepted_steps, mean),
        :accepted_steps_var  => data -> describe_pathlengths(data, s -> s.accepted_steps, var),
        :rejected_steps_mean => data -> describe_pathlengths(data, s -> s.rejected_steps, mean),
        :rejected_steps_var  => data -> describe_pathlengths(data, s -> s.rejected_steps, var),
        :steps_mean => data -> describe_pathlengths(data, s -> s.accepted_steps + s.rejected_steps, mean),
        :steps_var  => data -> describe_pathlengths(data, s -> s.accepted_steps + s.rejected_steps, var),
    ]
    collect_results!(
        fname,
        dir,
        white_list=wl,
        special_list=sl,
        verbose=verbose,
    )
end

# ## Loading the Data
#
# Unfortunately, using the `:total_degree` homotopy is not an option for larger
# `n` as the runtime gets to big. Hence, we restrict ourselves to `:polyhedral`
# start systems.

raw = collect_pathlengths!(datadir("raw"))
raw[!, :lifting] .= :none

cyclic = collect_pathlengths!(datadir("cyclic_lifting"))
cyclic[!, :lifting] .= :cyclic

range_spanning = collect_pathlengths!(datadir("range_spanning_lifting"))
range_spanning[!, :lifting] .= :range_spanning

data = vcat(raw, cyclic, range_spanning)
filter!(row -> row[:start_system] == :polyhedral, data)

nothing

# # Comparison
#
# The results don't look all that promising:
#
# * Cyclic lifting performs worst throughout.
# * For `n=10`, `k=2` the range spanning lifting has a reduced variance in the
#   number of accepted steps.
# * For `n=9`, `k=2` the range spanning lifting has a lower average of the
#   number of overall as well as rejected steps.
#
# The parameter `k` isn't shown in the plots, though.

cols = [:steps_mean          :steps_var
        :accepted_steps_mean :accepted_steps_var
        :rejected_steps_mean :rejected_steps_var]

ps = map(cols) do col
    plot(data, x = :n, y = col, color = :lifting)
end

set_default_plot_size(1000px, 1200px)
gridstack(ps)

# # Conclusion
#
# No lifting strategy looks very promising. Range spanning lifting seems to
# generally perform better than the cyclic lifting, even for prime `n`. This is
# contrary to the expectated complexity, which is better for the cyclic lifting
# strategy for prime `n`.
#
# Remaining questions/tasks:
#
# * Is there a connection between the expected complexity (cf.
#   `2019-10-30_expected-complexity.ipynb`) and the "path length" / "path
#   complexity"?
# * How to best estimate "path complexity"? In this report I naively used the
#   average number of steps needed. This is distorted by everything
#   `HomotopyContinuation` does under the hood. I should implement a custom
#   tracker object to actually measure the path length using some (Euclidean?)
#   metric, i.e. take the line integral.
# * I still need a way to find an "optimal" lifting for a given (set of)
#   strategies (without iterating all possible liftings or using Gröbner bases).
# * Measuring a single solution run (a single solve for each `n,k`) isn't quite
#   meaningful. I should take more data into account, i.e. repeat the
#   computations using several seeds. Maybe the range spanning lifting looks more
#   promising then.
# * What impact does the start system have on the measurements (`:total_degree`
#   vs `:polyhedral`)? To answer this I need an account for the new compute
#   server.

