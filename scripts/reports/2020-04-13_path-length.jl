# # Path Length
#
# Remaining questions from `2019-11-04_path-length.jl`:
#
# * Is there a connection between the expected complexity (cf.
#   `2019-10-30_expected-complexity.ipynb`) and the "path length" / "path
#   complexity"?
#
# * How to best estimate "path complexity"? In this report I naively used the
#   average number of steps needed. This is distorted by everything
#   `HomotopyContinuation` does under the hood. I should implement a custom
#   tracker object to actually measure the path length using some (Euclidean?)
#   metric, i.e. take the line integral.
#
# * I still need a way to find an "optimal" lifting for a given (set of)
#   strategies (without iterating all possible liftings or using Gröbner bases).
# * Measuring a single solution run (a single solve for each `n,k`) isn't quite
#   meaningful. I should take more data into account, i.e. repeat the
#   computations using several seeds. Maybe the range spanning lifting looks more
#   promising then.
#
# * What impact does the start system have on the measurements (`:total_degree`
#   vs `:polyhedral`)? To answer this I need an account for the new compute
#   server.
#
# Instead of measuring the number of steps a particular solver needed to solve
# a system, measure the actual path length.

using DrWatson, Parameters, Test
using HomotopyContinuation

using DataFrames
using StatsPlots

ns = 5:7
ks = 2:4

fname(n) = datadir("path-properties", "length", savename(@dict(n), "jld"))
fname(n, k, lifting) = datadir("path-properties", "length", lifting, savename(@dict(n,k), "jld"))

raw = [load(fname(n)) for n in ns]
clifted = [load(fname(n,k,"cyclic_lifting")) for n in ns, k in ks]
rlifted = [load(fname(n,k,"range_spanning_lifting")) for n in ns, k in ks]

nothing

# Validate that each lifting and (naive) path tracking found all the solutions:

lens(dict) = dict["lens"]
ls = map(lens, raw)
lsc = map(lens, clifted)
lsr = map(lens, rlifted)

@testset "found all solutions" begin
    for i in eachindex(ns)
        #TODO: Maybe solve reference system instead?
        @test all(length(lsc[i,j]) == length(ls[i]) for j in eachindex(ks))
    end
end

# How do the solutions compare?

function compare(idn, lifted_ls; ymax=100)
    p = StatsPlots.plot()
    for (idk,k) in enumerate(ks)
        data = filter(<=(ymax), lifted_ls[idn,idk])
        violin!(p, ["k=$k"], data, label="")
    end
    data = filter(<=(ymax), ls[idn])
    violin!(p, ["raw"], data, label="")
    plot!(p, ylims=(0,100), yticks=0:20:100)
    p
end

ps = [compare(i,l) for i in eachindex(ns), l in (lsc,lsr)]

for (i,n) in enumerate(ns)
    xlabel!(ps[i,end], "n=$n")
end
ylabel!(ps[1,1], "Cyclic Lifting")
ylabel!(ps[1,2], "Range Spanning Lifting")
title!(ps[2,1], "Path Length of Cyclic-n-Root Problem (capped at 100)")

#TODO: Why does `plot` transpose `ps`?
p = plot(ps..., layout=(2,3), size=(900,600))

# Overall, this supports the last observation that the unlifted system is the
# "best" candidate.
