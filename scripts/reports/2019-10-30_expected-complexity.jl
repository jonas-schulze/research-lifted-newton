# # Expected Complexity
#
# Let's have a look at the expected average runtime according to Lairez.

using DrWatson

import Pkg
Pkg.activate(projectdir())

#-

using JLD, HomotopyContinuation
using Parameters
using HomotopyContinuation: DynamicPolynomials

include(srcdir("cyclic_lifting.jl"))
include(srcdir("range_spanning_lifting.jl"))

function lairez_complexity(n, d)
    N = sum(binomial(n+_d, n) for _d in d)
    D = maximum(d)
    n * sqrt(D^3) * N
end

function lairez_complexity(F)
    n = union(variables.(F)...) |> length
    d = totaldegree.(F)
    lairez_complexity(n, d)
end

using DataFrames

function complexities(ns, fs)
    df = DataFrame(n=Int[], k=Int[], f=Function[], c=Float64[], D=Int[])
    for n in ns, f in fs
        ks = 2:n÷2
        Fs = [f(n, k) for k in ks] # systems
        Ds = [maximum(totaldegree.(F)) for F in Fs]
        cs = lairez_complexity.(Fs)
        append!(df, DataFrame(n=n, k=ks, f=f, c=cs, D=Ds))
    end
    df
end

#-

ns = 4:13
fs = (cyclic_lifting,range_spanning_lifting)
df = complexities(ns, fs)

by(df, [:n, :f]) do d
    i = argmin(d.c)
    (c=d.c[i], k=d.k[i], D=d.D[i])
end

# It looks like the range spanning lifting performs worse only for prime `n`.

using Primes

ns = primes(4, 30)
fs = (cyclic_lifting,range_spanning_lifting)
df = complexities(ns, fs)

by(df, [:n, :f]) do d
    i = argmin(d.c)
    (c=d.c[i], k=d.k[i], D=d.D[i])
end

# # Conclusion
#
# The two lifting strategies shown above represent two extremes of a family of
# strategies. The cyclic lifting with parameter `k` has an overlap of size
# `k-1` of the variables in its lifting binomials (e.g. `prod(x[1:3])` and
# `prod(x[2:4])` has an overlap of size 2, `prod(x[2:3])`). The range spanning
# lifting has an overlap of size 0. The former generally achieves a lower total
# degree of the system (lower `D`), the latter uses fewer variables (lower
# `n`). Hence, it's worth considering intermediate strategies having an overlap
# between 0 and `k-1`.
#
# Remaining questions:
#
# * How to represent a lifted system in a unique way (w/o Gröbner bases)?
# * How to find an optimal lifting (given a cost function like `lairez_complexity`)?

