# # Mixed Volume
#
# Let's have a look at the mixed volumes of some lifted systems.
#
# Disclaimer: it's the same as in the non-lifted system.

using DrWatson

import Pkg
Pkg.activate(projectdir())

#-

using JLD, HomotopyContinuation
using Parameters
using HomotopyContinuation: DynamicPolynomials
using PolynomialTestSystems: equations, cyclic

include(srcdir("cyclic_lifting.jl"))
include(srcdir("range_spanning_lifting.jl"))

nothing

# ## Cyclic Liftings

@doc cyclic_lifting

#-

equations(cyclic(4))

#-

cyclic_lifting(4, 2)

#-

function mixed_volumes(spec)
    @unpack n, ks = spec
    map(ks) do k
        F = cyclic_lifting(n, k)
        mixed_volume(F, show_progress=false)
    end
end

mixed_volumes(n, ks) = mixed_volumes(@dict n ks)

#-

map(4:10) do n
    ks = 2:n÷2
    vols = mixed_volumes(n, ks)
    n => unique(vols)
end

#-

# ## Range Spanning Liftings
#
# Instead of using all the cyclic lifting binomials, we restrict ourselves to
# the ones having disjoint supports. If necessary (e.g. for `n=8`, `k=3`), the
# we shrink "last" binomial to have fewer than `k` factors (`y[3]`).

F = range_spanning_lifting(8,3)

#-

d = totaldegree.(F)
D = maximum(d)

@show D, d;

# Due to the cyclic nature and the inability to express certain products, we
# expect some inefficiency in reducing the total degree of the system.

map(4:10) do n
    vols = map(2:n÷2) do k
        F = range_spanning_lifting(n, k)
        mixed_volume(F, show_progress=false)
    end
    n => unique(vols)
end

# # Conclusion
#
# It seems like as if lifting does not affect the mixed volume. Rizing questions:
#
# * Is this a general scheme (i.e. provable)?
# * Is this to be expected (is this even surprising)? TODO: look up definition
#   of mixed volume (in Bertini book or Verschelde thesis) -> different meaning
#   of lifting in Verschelde etc.

# # Wild Wild West
#
# We observe that the variables stored in the lifted system contain some with
# zero coefficients (falsely being considered part of the support of the
# system). Consider `n=5`: where the non-lifted system `G` contains all the
# variables in all the equations, its lifted system `F` has a different set of
# variables in each of the equations, equations 7 and 9 falsely report `x1` and
# `x2` being present in the equation.

G = equations(cyclic(5))

#-

variables.(G)

#-

using Base.Iterators: flatten

F = cyclic_lifting(5,2)

#-

@show variables(F)
variables.(F)

# So the question arises whether such a "false support" has an effect on the
# mixed volume being reported.
#
# We investigate an even simpler example: `n=3`. Next to the true system
# obtained from `PolynomialTestSystems`, we consider a "false support" in one
# additional variable, `b`, in all three equations. In order to fix the method
# being used to compute the mixed volume (maybe to not have higher-dimensional
# solutions), we add another equation fixing `b`.

@polyvar a[1:3] b

C3 = [
    sum(a) + b - b,
    a[1]*a[2] + a[2]*a[3] + a[3]*a[1] + b - b,
    prod(a) - 1 + b - b,
    b-1, # needed to evaluate mixed_volume
]

C3′ = copy(C3)
C3′[end] = b # or even sum(a) + b - b

C3

#-

variables.(C3)

#-

@show mixed_volume(equations(cyclic(3)))
@show mixed_volume(C3)
@show mixed_volume(C3′)
nothing

# We observe that `C3` indeed has the same mixed volume as `cyclic(3)`, but
# `C3′` has not. The latter is
#
# * somewhat obvious due to the singleton `(0,0,0,1)` in the Newton Polytope of
#   the last equation, causing the mixed volume to be zero;
# * somewhat confusing since both system contain the same amount of
#   "information", i.e. their solution sets are simply shiftings of one
#   another.
#
# # Further Conclusions
#
# * So maybe this is interesting after all, that the mixed volume (MV) stays
#   unaffected (because there are "stupid" liftings that alter the MV).
# * Unless I made a mistake calculating another example (which is always
#   possible), there are examples where a lifting alters the MV. -> Check for
#   scalings of `d!` where `d` is the dimension / number of variables in the
#   system.
# * Is it possible to define a MV for varieties? -> check Groebner Bases (GB)
#   for different monomial orders + universal GB

