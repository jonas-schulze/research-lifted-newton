JLs = $(wildcard scripts/reports/*.jl)
IPYNBs := $(JLs:.jl=.ipynb)
IPYNBs := $(notdir $(IPYNBs))
IPYNBs := $(addprefix notebooks/, $(IPYNBs))

.PHONY: all info notebook reports

JULIA = julia-1.3

all: reports

reports: $(IPYNBs)

info:
	@for f in $(IPYNBs); do echo $$f; done

notebook:
	$(JULIA) -e 'using IJulia; notebook(dir=".")'

notebooks/%.ipynb: scripts/reports/%.jl
	$(JULIA) -e 'import Literate; Literate.notebook("$<", "$(dir $@)", execute=false)'

