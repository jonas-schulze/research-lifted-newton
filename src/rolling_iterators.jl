import Base: iterate, length

struct Rolling{Itr,Buf}
    itr::Itr
    buf::Buf
end

function rolling(itr, n::Integer)
    buf = Vector{eltype(itr)}(undef, n)
    Rolling(itr, buf)
end

Base.length(b::Rolling) = length(b.itr) - length(b.buf) + 1

function Base.iterate(b::Rolling)
    len = length(b.buf)
    @assert len > 1

    vs = iterate(b.itr)
    isnothing(vs) && return nothing
    val, state = vs
    b.buf[1] = val
    vs = iterate(b.itr, state)

    i = 2
    while !isnothing(vs)
        val, state = vs
        b.buf[i] = val
        i == len && break
        i += 1
        vs = iterate(b.itr, state)
    end

    i < len && return nothing
    (b.buf, state)
end

function Base.iterate(b::Rolling, state)
    vs = iterate(b.itr, state)
    isnothing(vs) && return nothing
    val, state = vs
    len = length(b.buf)
    @views b.buf[1:len-1] = b.buf[2:len]
    b.buf[len] = val
    (b.buf, state)
end
