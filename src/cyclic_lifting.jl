using Parameters
using HomotopyContinuation
using HomotopyContinuation.DynamicPolynomials: Monomial, Polynomial
using Base.Iterators: partition

"""
    cyclic_lifting(n, k) -> h

Generate the cyclic-`n`-root problem having groups of `k` successive variables
replaced by a newly created one. Thus, the system contains `2n` polynomials,
including the polynomials fixing the lifting/replacement of the aforementioned
`k`-ranges.
"""
function cyclic_lifting(n, k)
  cyclic_lifting(@ntuple n k)
end

function cyclic_lifting(spec)
  @unpack n, k = spec
  @assert 1 < k < n

  @polyvar x[1:n] y[1:n]

  # Add lifting variables:
  h = [prod(j -> x[mod1(j,n)], i:i+k-1)-y[i] for i in 1:n]
  sizehint!(h, 2n)

  # Add non-lifted polynomials:
  for i in 1:k-1
    p = sum(prod(x[mod1(_k,n)] for _k in j:j+i-1) for j in 1:n)
    push!(h, p)
  end

  # Add lifted polynomials:
  for i in k:n-1
    p = zero(first(h))
    for j in 1:n
      m = one(Monomial{true})
      for range in partition(j:j+i-1, k)
        if length(range) == k
          m *= y[mod1(first(range),n)]
          continue
        end
        for _k in range
          m *= x[mod1(_k,n)]
        end
      end
      p += m
    end
    push!(h, p)
  end

  # Add final, lifted polynomial:
  m = one(Monomial{true})
  for range in partition(1:n, k)
    if length(range) == k
      m *= y[first(range)]
      continue
    end
    m *= prod(x[r] for r in range)
  end
  push!(h, m-1)
  h
end
