using DrWatson, JLD, HomotopyContinuation
using Parameters
using PolynomialTestSystems: cyclic

const HC = HomotopyContinuation

function solve_raw_problem(spec)
  @unpack n, start_system = spec

  seed = try
           @unpack seed = spec
         catch
           HC.randseed()
         end

  F = cyclic(n).equations
  results = solve(F, start_system=start_system, seed=seed)

  @strdict spec results
end
