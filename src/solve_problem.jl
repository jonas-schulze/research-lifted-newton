using DrWatson, JLD, HomotopyContinuation
using Parameters

const HC = HomotopyContinuation

function solve_problem(spec)
  @unpack F, start_system = spec

  seed = try
           @unpack seed = spec
         catch
           HC.randseed()
         end

  results = solve(F, start_system=start_system, seed=seed)

  @strdict spec results
end
