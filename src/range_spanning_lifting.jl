using HomotopyContinuation.DynamicPolynomials
using Parameters
using Compat: only

"""
    lift(p::Polynomial, b::Polynomial) -> q::Polynomial

Divide all monomials of `p` by the binomial `b`, if possible.
"""
function lift(p, b)
    x, y = monomials(b)
    lifted = map(monomials(p)) do m
        divides(x, m) || return m
        d = only(monomials(m ÷ x))
        d*y
    end
    Polynomial(coefficients(p), lifted)
end

function range_spanning_lifting(n, k)
    @assert 1 < k < n

    l = ceil(Int, n/k)
    @polyvar x[1:n] y[1:l]

    # Add lifting variables:
    B = [prod(x[k*(i-1)+1:k*i]) - y[i] for i in 1:l-1]
    push!(B, prod(x[k*(l-1)+1:end]) - y[end]) # TODO: remove this one, if it's $y_l = x_n$

    # Non-lifted polynomials:
    F = map(1:n-1) do i
        sum(prod(x[mod1(_k,n)] for _k in j:j+i-1) for j in 1:n)
    end
    push!(F, prod(x)-1)

    # Lift polynomials
    F = map(F) do f
        for b in B
            f = lift(f, b)
        end
        f
    end

    vcat(B, F)
end

function range_spanning_lifting(spec)
    @unpack n, k = spec
    range_spanning_lifting(n, k)
end


totaldegree(F) = maximum(degree(m) for m in monomials(F))
