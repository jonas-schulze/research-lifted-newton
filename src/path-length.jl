using DrWatson
using HomotopyContinuation
using HomotopyContinuation: randseed
using LinearAlgebra
using Parameters

include(srcdir("rolling_iterators.jl"))

function pathlen(path)
    len = 0.0
    rdot = real ∘ dot
    for (xt1, xt2) in rolling(path, 2)
        x1, t1 = xt1
        x2, t2 = xt2
        Δx = x1 - x2
        Δt = t1 - t2
        len += sqrt(rdot(Δx, Δx) + rdot(Δt, Δt))
    end
    len
end

function pathlens(F; seed=randseed())
    lens = Float64[]
    tracker, startsols = coretracker_startsolutions(F, max_step_size=0.01, seed=seed)
    for xstart in startsols
        xts = iterator(tracker, xstart)
        len = pathlen(xts)
        status(tracker) == CoreTrackerStatus.success || continue
        push!(lens, len)
    end
    lens
end
