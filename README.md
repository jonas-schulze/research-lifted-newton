This is an awesome new scientific project that uses `DrWatson`!

# Usage

Make sure to always activate the local environment. There are several ways to
do so:

* from within Julia: `(old-env) pkg> activate .`
* start Julia using `$ julia --project=@.` or `$ JULIA_PROJECT=@. julia`
* `export JULIA_PROJECT=@.` in your `.bashrc`, `.zshenv` or similar (my recommendation)

When activating the environment for the first time, you'll also need to
instantiate it: `pkg> instantiate`. This will ensure that Julia loads all the
Modules (DrWatson, etc.) in their proper version.
